# prueba_tecnica_pragma

A new Flutter project.

# Flutter App

## Descripción
Esta aplicación está desarrollada con Flutter, un SDK de código abierto para crear aplicaciones nativas en iOS, Android, web y escritorio.

## Requisitos previos
1. [Flutter](https://flutter.dev/docs/get-started/install) SDK instalado.
2. [Dart](https://dart.dev/get-dart) instalado.
3. Un editor de código como [Visual Studio Code](https://code.visualstudio.com/) o [Android Studio](https://developer.android.com/studio).

## Instalación

1. **Clonar el repositorio:**
    ```bash
    git clone https://gitlab.com/juansebas12/pruebatecnicapragma.git
    ```

2. **Instalar dependencias:**
    ```bash
    flutter pub get
    ```

3. **Ejecutar la aplicación:**
    - En un dispositivo físico o emulador:
        ```bash
        flutter run
        ```

4. **Compilar para producción:**
    - Android:
        ```bash
        flutter build apk
        ```
    - iOS:
        ```bash
        flutter build ios
        ```
