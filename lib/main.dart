import 'package:flutter/material.dart';
import 'package:prueba_tecnica_pragma/pages/Splash/splash.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textSelectionTheme: const TextSelectionThemeData(cursorColor: Colors.white),
      ),
      home: const SplashScreen(),
    );
  }
}
