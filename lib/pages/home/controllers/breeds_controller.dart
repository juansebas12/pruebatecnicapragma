import 'package:rxdart/rxdart.dart';
import 'package:prueba_tecnica_pragma/pages/details_breed/models/breed.dart';
import 'package:prueba_tecnica_pragma/pages/details_breed/models/image.dart';
import 'package:prueba_tecnica_pragma/pages/home/providers/breeds_provider.dart';

class BreedsController {
  BehaviorSubject<List<Breed>> breedListCollection = BehaviorSubject<List<Breed>>();
  List<Breed> listBreeds = [];

  BehaviorSubject<bool> loadingCollection = BehaviorSubject<bool>();
  bool loadingData = false;

  String filterText = "";

  final breedProvider = BreedProvider();

  getBreedsFromApi() async {
    final response = await breedProvider.getBreedsApi();
    listBreeds = (response as List?)?.map((dynamic e) => Breed.fromJson(e as Map<String, dynamic>)).toList() ?? [];
    breedListCollection.sink.add(listBreeds);
    loadingData = true;
    loadingCollection.sink.add(loadingData);
    getImagesBreedById();
  }

  getImagesBreedById() async {
    for (var i = 0; i < listBreeds.length; i++) {
      breedProvider.getImageBreedById(listBreeds[i].id ?? "").then((response){
        listBreeds[i].images = (response as List?)?.map((dynamic e) => Images.fromJson(e as Map<String, dynamic>)).toList() ?? [];
        filterBreedsByText(filterText);
      });
    }
  }

  filterBreedsByText(String text) {
    filterText = text;
    if (text == "") {
      breedListCollection.sink.add(listBreeds);
    } else {
      List<Breed> listBreedsFilter =
          listBreeds.where((element) => element.name!.toLowerCase().contains(text.toLowerCase())).toList();
      breedListCollection.sink.add(listBreedsFilter);
    }
  }

  Stream<List<Breed>> get breedListStream => breedListCollection.stream;
  Stream<bool> get loadingStream => loadingCollection.stream;
}
