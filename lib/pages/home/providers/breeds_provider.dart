import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:http/http.dart';

class BreedProvider {
  getBreedsApi() async {
    Response httpResponse = await http.get(Uri.parse("https://api.thecatapi.com/v1/breeds"),
        headers: {'x-api-key': "bda53789-d59e-46cd-9bc4-2936630fde39"});
    if (httpResponse.statusCode == 200) {
      return json.decode(utf8.decode(httpResponse.bodyBytes));
    }
    return null;
  }

  getImageBreedById(String breedId) async {
    try {
      Response httpResponse = await http.get(Uri.parse("https://api.thecatapi.com/v1/images/search?breed_ids=$breedId"),
          headers: {'x-api-key': "bda53789-d59e-46cd-9bc4-2936630fde39"});
      if (httpResponse.statusCode == 200) {
        return json.decode(utf8.decode(httpResponse.bodyBytes));
      }
      return null;
    } catch (e) {
      return null;
    }
  }
}
