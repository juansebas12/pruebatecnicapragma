import 'package:flutter/material.dart';
import 'package:prueba_tecnica_pragma/common/styles/Text_field_style.dart';
import 'package:prueba_tecnica_pragma/common/styles/custom_theme.dart';
import 'package:prueba_tecnica_pragma/pages/details_breed/models/breed.dart';
import 'package:prueba_tecnica_pragma/pages/home/controllers/breeds_controller.dart';
import 'package:prueba_tecnica_pragma/pages/home/widgets/card_breed.dart';

class HomeBreeds extends StatelessWidget {
  final BreedsController breedsController = BreedsController();

  HomeBreeds({super.key}) {
    breedsController.getBreedsFromApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlue,
        leading: const SizedBox.shrink(),
        title: const Text(
          "Catbreeds",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w700,
            fontSize: 18.0,
          ),
        ),
      ),
      body: StreamBuilder<bool>(
        stream: breedsController.loadingStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData || snapshot.data == false) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return listBeerd();
        },
      ),
    );
  }

  Widget listBeerd() {
    return StreamBuilder<List<Breed>>(
      stream: breedsController.breedListStream,
      builder: (context, snapshot) {
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0, top: 16.0),
              child: TextFormField(
                style: TextFieldStyle.textStyle(colorText: CustomTheme().colors.lightBlue),
                onChanged: (value) {
                  breedsController.filterBreedsByText(value);
                },
                maxLines: 1,
                decoration: TextFieldStyle.withIcon(
                  labelText: "Buscar",
                  leftIcon: Icon(
                    Icons.search,
                    color: CustomTheme().colors.darkBlue,
                  ),
                ),
                cursorColor: CustomTheme().colors.darkBlue,
              ),
            ),
            Expanded(
              child: ListView.separated(
                padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                shrinkWrap: true,
                itemCount: snapshot.data?.length ?? 0,
                separatorBuilder: (BuildContext context, int index) {
                  return const SizedBox(
                    height: 16.0,
                  );
                },
                itemBuilder: (BuildContext context, int index) {
                  return CardBreed(breed: snapshot.data![index]);
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
