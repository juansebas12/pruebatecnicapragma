import 'package:flutter/material.dart';
import 'package:prueba_tecnica_pragma/common/redirect_service.dart';
import 'package:prueba_tecnica_pragma/common/styles/custom_theme.dart';
import 'package:prueba_tecnica_pragma/pages/details_breed/details_breed.dart';
import 'package:prueba_tecnica_pragma/pages/details_breed/models/breed.dart';

class CardBreed extends StatelessWidget{

  final Breed breed;

  const CardBreed({super.key, required this.breed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        RedirectService.goTo(
            context,
            DetailBreed(
              breed: breed,
            ));
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: CustomTheme().colors.black, width: 1.0),
        ),
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${breed.name}',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: CustomTheme().colors.black,
                    fontSize: 14.0,
                  ),
                ),
                Text(
                  'Más...',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: CustomTheme().colors.darkBlue,
                    fontSize: 14.0,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: breed.images!.isEmpty
                  ? const CircularProgressIndicator()
                  : Image.network(
                      '${breed.images?[0].url}',
                      fit: BoxFit.cover,
                      height: 350.0,
                      width: double.infinity,
                    ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${breed.origin}',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: CustomTheme().colors.black,
                    fontSize: 14.0,
                  ),
                ),
                Text(
                  '${breed.temperament?.split(',')[0]}',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: CustomTheme().colors.black,
                    fontSize: 14.0,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

}