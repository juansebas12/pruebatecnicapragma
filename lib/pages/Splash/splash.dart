import 'dart:async';

import 'package:flutter/material.dart';
import 'package:prueba_tecnica_pragma/common/redirect_service.dart';
import 'package:prueba_tecnica_pragma/common/styles/custom_theme.dart';
import 'package:prueba_tecnica_pragma/pages/home/home.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    Timer(const Duration(seconds: 1), () {
      RedirectService.replaceAllPages(context, HomeBreeds());
    });
    return Scaffold(
      backgroundColor: CustomTheme().colors.darkBlue,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const Text(
              "CATBREEDS",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w500,
                fontSize: 50.0,
                shadows: [
                  Shadow(
                    blurRadius: 10.0,
                    color: Colors.black,
                    offset: Offset(3.0, 3.0),
                  ),
                ],
              ),
            ),
            Image.asset(
              "assets/images/cat.png",
              width: size.width * 0.68,
            ),
          ],
        ),
      ),
      // SizedBox(
      //   height: size.height,
      //   child: Image.asset(
      //     "assets/images/Catbreeds.png",
      //     filterQuality: FilterQuality.low,
      //     // fit: BoxFit.cover,
      //     width: size.width,
      //     height: size.height,
      //   ),
      // ),
    );
  }
}
