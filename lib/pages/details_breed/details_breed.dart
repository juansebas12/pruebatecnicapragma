import 'package:flutter/material.dart';
import 'package:prueba_tecnica_pragma/pages/details_breed/models/breed.dart';
import 'package:prueba_tecnica_pragma/common/redirect_service.dart';
import 'package:prueba_tecnica_pragma/common/styles/custom_theme.dart';

class DetailBreed extends StatelessWidget {
  final Breed breed;

  const DetailBreed({super.key, required this.breed});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlue,
        leading: InkWell(
          onTap: () {
            RedirectService.pop(context);
          },
          child: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        title: Text(
          "${breed.name}",
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w700,
            fontSize: 18.0,
          ),
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Image.network(
              '${breed.images?[0].url}',
              fit: BoxFit.cover,
              width: size.width * 0.95,
              height: size.width * 0.95,
            ),
          ),
          Expanded(
              child: ListView(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 20.0),
            children: [
              itemText(breed.description ?? ""),
              itemText('pais de origen : ${breed.origin}'),
              itemText('temperamento : ${breed.temperament}'),
              itemText('tiempo de vida : ${breed.lifeSpan}'),
              itemText('adaptabilidad : ${breed.adaptability}'),
              itemText('nivel de afecto : ${breed.affectionLevel}'),
              itemText('apto para niños : ${breed.childFriendly}'),
              itemText('apto para perros : ${breed.dogFriendly}'),
              itemText('nivel de energía : ${breed.energyLevel}'),
              itemText('cuestiones sanitarias : ${breed.healthIssues}'),
              itemText('inteligencia : ${breed.intelligence}'),
              itemText('nivel de desprendimiento : ${breed.sheddingLevel}'),
              itemText('necesidades sociales : ${breed.socialNeeds}')
            ],
          ))
        ],
      ),
    );
  }

  Widget itemText(String item) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 8.0,
        ),
        Text(
          item,
          style: TextStyle(
            fontWeight: FontWeight.w500,
            color: CustomTheme().colors.black,
          ),
        ),
      ],
    );
  }
}
