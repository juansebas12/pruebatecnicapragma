class Weight {
  final String? imperial;
  final String? metric;

  Weight({
    this.imperial,
    this.metric,
  });

  Weight.fromJson(Map<String, dynamic> json)
      : imperial = json['imperial'] as String?,
        metric = json['metric'] as String?;

  Map<String, dynamic> toJson() => {'imperial': imperial, 'metric': metric};
}
