class Images {
  final String? id;
  final String? url;
  final int? width;
  final int? height;

  Images({
    this.id,
    this.url,
    this.width,
    this.height,
  });

  Images.fromJson(Map<String, dynamic> json)
      : id = json['id'] as String?,
        url = json['url'] as String?,
        width = json['width'] as int?,
        height = json['height'] as int?;

  Map<String, dynamic> toJson() => {'id': id, 'url': url, 'width': width, 'height': height};
}
